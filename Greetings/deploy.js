Web3 = require('web3');
solc = require('solc');
fs = require('fs');

web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));

// Read the solidity file & compile it
sourceCode = fs.readFileSync('./Greetings.sol').toString();
compiledCode = solc.compile(sourceCode);

// Deploy contract to the blockchain
contractABI = JSON.parse(compiledCode.contracts[':Greetings'].interface);

greetingsContract = web3.eth.contract(contractABI);

byteCode = compiledCode.contracts[':Greetings'].bytecode;

greetingsDeployed = greetingsContract.new({
  data: byteCode,
  from: web3.eth.accounts[0],
  gas: 4700000
});

/**
 * Somehow the address is not returned when executed in this file.
 * But if executed via node CLI the address is returned after a short time
 * To be investigated (ask Q to the site Udemy )
 */
console.log('Deployed address: ' + greetingsDeployed.address);

// Get instance to the contract in the blockchain
const greetingsInstance = greetingsContract.at(greetingsDeployed.address);
console.log('Greeting: ' + greetingsInstance.getGreetings());

// Set and display the new greeting value
greetingsInstance.setGreetings('Hello ChainSkillS!', { from: web3.eth.accounts[0] });
console.log('Greeting: ' + greetingsInstance.getGreetings());
